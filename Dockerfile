FROM rust:latest
RUN rustup target add aarch64-unknown-linux-gnu
RUN apt-get update && apt-get install -y gcc make gcc-aarch64-linux-gnu binutils-aarch64-linux-gnu
